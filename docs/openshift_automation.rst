.. include:: refs.rst

Automation in OpenShift
=======================

An up and running OpenShift is ready along with the completion of CORD POD deployment. 

OpenShift platform is the core of the whole installation process, so it will be installed in the first step of the installation. Once the OpenShift is ready, the remaining installation processes are all controlled by it.

Log in OpenShift
----------------

You can log in OpenShift via the following two ways:

1. `Command Line Interface (OpenShift client) <https://github.com/openshift/origin/releases>`_

2. `OpenShift Web Console <https://docs.openshift.org/latest/architecture/infrastructure_components/web_console.html>`_

Default admin login credential is ``system`` / ``admin``


Start with Different Pipelines
------------------------------

All the automation tasks are driven by the OpenShift Pipeline(controlled by Jenkins). Every project has one or many pipelines to serve different purposes, for example:

- Deploy OS only

+ CORD POD(rcord) Deployment

- CORD POD(mcord) Deployment

+ CORD POD API Test

- CORD POD(rcord) Deployment + API Test

+ CORD POD(mcord) Deployment + API Test

As shown above, a pipeline could be any combination of atomic tasks which is defined by the users.

Also, there are two ways to start pipelines:

Command Line Interface
>>>>>>>>>>>>>>>>>>>>>>

You can get pipeline list by using the following command:

.. code-block:: sh

    # log in OpenShift
    $ oc login https://[your_openshift_portal]

    # switch to the project you are using
    $ oc project [your_project_name]

    # get pipeline list
    $ oc get buildconfig | grep JenkinsPipeline | awk '{print $1}'

    # start pipeline
    $ oc start [pipeline_name]



Web Console
>>>>>>>>>>>

Log in OpenShift via a vaild credential, then browse to:

    **Project** -> **Builds** -> **Pipilines**

You should be able to see the web page which is like the following image:

|OpenShift Pipeline Demo Page|

Then, start the pipeline you want.



.. disqus::