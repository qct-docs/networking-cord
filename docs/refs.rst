.. _Openshift: https://www.openshift.org/

.. _Intel I350-T4: https://www.intel.com/content/www/us/en/support/products/59063/network-and-i-o/ethernet-products/intel-gigabit-desktop-adapters/intel-ethernet-server-adapter-i350-series/intel-ethernet-server-adapter-i350-t4.html

.. _Mellanox ConnectX4: http://www.mellanox.com/page/products_dyn?product_family=214&mtag=connectx_4_lx_en_ic

.. _QCT GitLab: http://git.qctrd.com


.. |OpenShift Pipeline Demo Page| image:: images/openshift_pipeline_demo_page.png