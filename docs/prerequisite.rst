Prerequisite
^^^^^^^^^^^^

Internet Access
----------------

Internet access is a must in a CORD POD installation. An internet-accessible
subnet information is needed for configuring the VMs described below.


2 VMs
-----

There are two VMs needed for the whole installation process:

- **OpenShift platform** and **PXE server** (with Ubuntu 16.04 installed)

+ **CORD bootstrap node** (from a pre-validated VM image)

  For the time begin, the OS of CORD bootstrap node is Ubuntu 14.04 (Trusty)


And there are some configurations needs to be done in above 2 VMs:

- A snapshot named ``initial`` must be taken once OS installation is done

+ Configure the login credential to ``cord`` (user name) / ``password`` (password)

- user ``cord`` should be a sudoer

.. tip::

  Fully automation could be implemented in a laptop with Ubuntu installed. If you use Windows or MAC OS, you need to manually prepare above VMs and power them on before executing deployment script.


BMC Preconfigured
-----------------

BMC IP addresses of all the servers need to be pre-configured.

.. todo::

  All the BMC IP addresses configuration will be included in the automatic installation processes in the future.

