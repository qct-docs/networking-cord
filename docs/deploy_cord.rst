Deploy CORD POD
===============

Once all the prerequisites are all ready, we can start jumping into the installation processes.

Please follow the steps described below to finish installation task.


Power On VMs
------------

Power on the VM which is used to be the OpenShift platform and PXE server, and go to the next step once the booting process is done.

Virtualization Platform Support
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

The following virtualization platforms are supported to deploy CORD POD in a fully automatic way:

- standalone ESXi Host

If the VM is installed in ssh-enabled standalone ESXi host, this step could be skipped.

    **Due to the automation processes are controlled by Ansible, SSH service is a must for the virtualization platforms listed above.**


Clone Git Repository
--------------------

Please login in an Ubuntu Linux environment and execute the following commands:

.. code-block:: sh

    $ sudo apt-get update && sudo apt-get -y install git

    $ git clone http://git.qctrd.com/softdev/portable-openshift-origin.git

    $ sudo bash portable-openshift-origin/init-sit.sh

Then all the installation processes will be finished by itself. There are 4 main steps in the whole process, and the time consumption estimations of every step are listed below:

+------+-----------------------------+-----------------+
| Step | Task                        | Time Estimation |
+======+=============================+=================+
| 1    | Install OpenShift           | 10 mins         |
+------+-----------------------------+-----------------+
| 2    | Deploy OpenStack Objects    | 1 min           |
+------+-----------------------------+-----------------+
| 3    | Deploy CORD POD             | 2 hours 10 mins |
+------+-----------------------------+-----------------+
| 4    | CORD POD Functionality Test | 10 mins         |
+------+-----------------------------+-----------------+

CORD POD should be up and running once the shell script execution is finished.

And if you would like to check the deployment status, you can go to `OpenShift Pipeline Page <https://os-portal.qctrd.com:8443/console/project/cord-qct-sit/browse/pipelines>`_ to see the deployment status.


.. disqus::
