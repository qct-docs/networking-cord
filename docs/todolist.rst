To Do List
==========

Log 儲存
-------

.. todo::

  - 儲存上次佈署時產生 Log 的位置 (從 Jenkines Groovy 產生，丟到 etcd service)

  + 需要存 log 前到 etcd service 查詢應該存放的 log 位置





自動化取得 HW MAC address
-----------------------

驅動 CI/CD automation 需要 HW MAC address，包含：

1. switch management port

#. Head node PXE interface

#. Compute node 


#. external interface

::

  這個若是 SKU 定下來應該會是固定不變的

#. fabric interface

::

  這個若是 SKU 定下來應該會是固定不變的


Compute Node
------------

- 如何自動化判斷接在哪個 leaf switch ?

+ `cord prov list` 顯示的未必是 PXE MAC Addr(因此要透過計算 MAC 為 +- 4 來判斷，需要額外寫 Ansible module)


Notification
------------

- 避免需要進入 Jenkins 設定，想辦法以程式化的方式解決


縮減所需 VM 數量
--------------

Portable OpenShift Platform + PXE server 可以放在一起

CORD bootstrap 單獨進行版本管理


OpenShift
---------

- 將 build config / deployment / pipeline ... 等物件的建立變成 template