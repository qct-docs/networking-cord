.. include:: .special.rst
.. include:: refs.rst


Prepare Installation Configuration
==================================

QCT designed CI/CD processes to install CORD ready POD solution in a fully automatic way which is driven by `OpenShift`_. And this section describes how to prepare the installation configurations of OpenShift for deploying the CORD POD automatically.


Collect Hardware Information
----------------------------

MAC address
>>>>>>>>>>>

Some hardware MAC addresses needed to be collected for deploying CORD POD, they are listed below:

+ External interface (:red-emphasize:`1st` port of `Intel I350-T4`_ of head node)

- Management interface (:red-emphasize:`2nd` port of `Intel I350-T4`_ of head node & :red-emphasize:`1st` port of `Intel I350-T4`_ of compute node)

+ Fabric interface (:red-emphasize:`1st` port of `Mellanox ConnectX4`_)

- Switch management port

.. todo::

  Hardware MAC address discovery will be included in the automatic installation processes in the future.


Switch Cabling Configuration
>>>>>>>>>>>>>>>>>>>>>>>>>>>>

Here are some examples of switch cabling configurations:

.. code-block:: yaml

  SWITCHES:
  - MGNT_MAC: "a8:1e:84:a5:a1:0d"
    IP: 10.6.0.51
    ROLE: "spine"
    CABLING_CONFIG:
    - "31=1x40g"
    - "32=1x40g"
  - MGNT_MAC: "a8:1e:84:a5:9f:7e"
    IP: 10.6.0.52
    ROLE: "spine"
    CABLING_CONFIG:
    - "31=1x40g"
    - "32=1x40g"
  - MGNT_MAC: "a8:1e:84:a5:97:b3"
    IP: 10.6.0.53
    ROLE: "leaf"
    CABLING_CONFIG:
    - "23=4x25g"
    - "31=1x40g"
    - "32=1x40g"
  - MGNT_MAC: "a8:1e:84:a5:a2:9c"
    IP: 10.6.0.54
    ROLE: "leaf"
    CABLING_CONFIG:
    - "23=4x25g"
    - "31=1x40g"
    - "32=1x40g"



CORD Deployment Configuration
-----------------------------

.. role:: raw-html(raw)
   :format: html

CORD CI/CD environment relies on the following two Git projects to move:

- **http://git.qctrd.com/softdev/pxe-boot.git**

+ **http://git.qctrd.com/networking/automation-prototype.git**

And every set of CORD hardware environment has its corresponding configuration in above two projects. So it's needed to prepare correct configurations for each set of CORD hardware environment, and the configurations are located in the following paths in projects:

Here uses project name :blue-emphasize:`cord-qct-sit` as the example:


**softdev/pxe-boot**

- /ansible/inventory/:blue-emphasize:`cord-qct-sit`.ini


**networking/automation-prototype**

- /openshift-objects/preq/:blue-emphasize:`cord-qct-sit`/build-config.yml

+ /openshift-objects/preq/:blue-emphasize:`cord-qct-sit`/image-stream.yml

- /openshift-objects/preq/:blue-emphasize:`cord-qct-sit`/rest-api.yml

+ /openshift-objects/preq/:blue-emphasize:`cord-qct-sit`/service-etcd.yml

- /ansible/roles/role_bootstrap-localrepo/files/cord/build/maas/roles/maas/files/:blue-emphasize:`cord-qct-sit`\_dhcpd.reservations

+ :red-emphasize:`/ansible/group_vars/cord-qct-sit.yml` (this is the main configuration for controlling the CI/CD tasks)

.. todo::

  **Above configurations(despite of the last one) will be generated automatically by OpenShift template in the future.**


Once all the hardware environment configurations are all ready, it's good to go.



.. disqus::
