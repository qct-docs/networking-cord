Effection Demo
==============

.. include:: ../.special.rst


Pull-quote
----------

.. pull-quote::

   Just as a solid ...


Epigraph and highlights
-----------------------

.. highlights::

   With these *highlights* ...


.. epigraph::

   No matter where you go, there you are.

   -- Buckaroo Banzai


Admonitions
-----------

.. attention::

  this is attention.


.. caution::

  this is caution


.. danger::

  this is danger


.. error::

  this is error


.. hint::

  this is hint


.. important::

  this is important


.. note::

  this is note


.. tip::

  this is tip


.. warning::

  this is warning


.. admonition::

  this is admonition



Font Color
----------

This is :red-emphasize:`red !` And :blue-emphasize:`this part is blue`.