.. Read the Docs Template documentation master file, created by
   sphinx-quickstart on Tue Aug 26 14:19:49 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

QCT CORD POD 5.0 Installation Guide
===================================

  **This document is currently under development, all the contents might be changed any time.**


This document is the installation guilde of CORD POD 5.0.

current version: 3.0


.. toctree::
  :maxdepth: 2
  :caption: Table of Contents

  todolist

  authors

  hardware_req

  prerequisite

  prepare_install_config

  deploy_cord

  openshift_automation

  functional_test

  help

  demo/graphviz

  demo/effect.rst

