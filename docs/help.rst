Get Help
========

reStructuredText Syntax
-----------------------

- `vscode reStructuredText Live Preview <https://github.com/vscode-restructuredtext/vscode-restructuredtext/blob/master/docs/sphinx.md>`_

+ `reStructuredText(rst)快速入門語法說明 - IT閱讀 <http://www.itread01.com/articles/1475823849.html>`_

- `reStructuredText cheatsheet <https://github.com/ralsina/rst-cheatsheet/blob/master/rst-cheatsheet.rst>`_

+ `Quick reStructuredText <http://docutils.sourceforge.net/docs/user/rst/quickref.html>`_

- `reStructuredText Primer — Sphinx 1.8.0+ documentation <http://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_

+ `reStructuredText Manual — myhome 1.0.0 documentation <http://conderls.github.io/homepage/reST.html>`_

- `轻量级标记语言 — GotGitHub <http://www.worldhello.net/gotgithub/appendix/markups.html>`_

+ `【文檔】使用Sphinx + reST編寫文檔 - 掃文資訊 <https://hk.saowen.com/a/bcbe0bee0d66b1120652a24eaeac68236d48054a5dd30ebb83501404dc6bfbaf>`_


Extensions
----------

- `reStructuredText Primer — Sphinx 1.8.0+ documentatio <http://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_



Test
----

	
.. role:: raw-html(raw)
   :format: html

用新定义的role插入换行，
:raw-html:`<br />`
:raw-html:`<font color='red'><b>`
本行不再斷行! 且標記為紅色。.
:raw-html:`</b></font />`